# как использовать
# python 3  (https://www.python.org/)
# pip install python-twitter (https://pypi.python.org/pypi/python-twitter)
# pip install requests       (https://github.com/psf/requests)
# pip install playsound      (https://pypi.org/project/playsound/)
# Bot lite v0.0.3 by DDevel
# WAVES: 3PAQiFpJwUUDcDDszDzWWRqTxf2x2Pd3U8Z
# (Если есть желание, можете задонатить.)
# Есть идеи и советы, как улучшить, пишите lexa-skripa@rambler.ru
# p.s. код не совершенен (мне стыдно, но он работает)

import twitter
import playsound
import requests
import hashlib
import json
import time
import getopt
import os
import sys

NodeURL = "https://nodes.wavesplatform.com"  # не трогать
Address = "ваш_кошелек"
Limit = "25"  # кол-во ордеров - 100 максимальное значение

# Звуковое оповещение
Used_Sound = False
Sound = "ваша_мелодия.mp3"

# Twitter api
Used_Twitter = False
UserId = ''  # цифры из AccessToken
ApiKey = ''
ApiSecretKey = ''
AccessToken = ''
AccessTokenSecret = ''

orders_DB = []
hash_DB = None


def postTwitt(text='', user_id=UserId):
    global ApiKey
    global ApiSecretKey
    global AccessToken
    global AccessTokenSecret

    api = twitter.Api(consumer_key=ApiKey, consumer_secret=ApiSecretKey,
                      access_token_key=AccessToken,
                      access_token_secret=AccessTokenSecret,
                      input_encoding=None)
    try:
        api.PostDirectMessage(text, user_id)
    except UnicodeDecodeError:
        print("Your message could not be encoded. \
             Perhaps it contains non-ASCII characters?")
        print("Try explicitly specifying the encoding \
             with the --encoding flag")
        sys.exit(2)


# GET /assets/{id} Get asset info by asset ID
def getAssetsID(id="", nodeURL="https://api.wavesplatform.com", api="v0"):
    if(id == "" or id is None):
        id = "WAVES"
    r = requests.get(nodeURL+"/"+api+"/assets/"+id)
    res = json.loads(r.text)
    # print(str(r.text))
    return res


# GET /transactions/address/{address}/limit/{limit}
def getOrders(address=Address, nodeURL=NodeURL, limit=Limit):
    r = requests.get(nodeURL+"/transactions/address/"+address+"/limit/"+limit)
    res = json.loads(r.text)
    # print(str(r.text))
    return res


# GET /assets/details/{assetId}
def getDetailsAssetId(assetId="", nodeURL=NodeURL):
    r = requests.get(nodeURL+"/assets/details/"+assetId)
    res = json.loads(r.text)
    # print(str(r.text))
    return res


def getCountStr(assetId="", count=0):
    # print("assetId = "+str(assetId))
    if (count != 0):
        if (assetId is None):
            res = {'decimals': 8}
        else:
            res = getDetailsAssetId(assetId)

        str_count = str(count)
        decimals = int(res['decimals'])
        # print("decimals = "+str(decimals))

        if (decimals == len(str_count)):
            return "0."+str_count
        if (decimals < len(str_count)):
            return str_count[0:len(str_count) - decimals] + "." + \
                   str_count[len(str_count)-decimals:]
        if (decimals > len(str_count)):
            return "0." + "0" * (decimals - len(str_count)) + str_count
    else:
        return "0"


def getPriceStr(amountAssetId="", priceAssetId="", count=0):
    # print("amountAssetId = "+str(amountAssetId))
    # print("priceAssetId = "+str(priceAssetId))

    amountDecimals = 0
    priceDecimals = 0

    if (count != 0):
        if (amountAssetId is None):
            amountDecimals = 8
        else:
            res = getDetailsAssetId(amountAssetId)
            amountDecimals = int(res['decimals'])

        if (priceAssetId is None):
            priceDecimals = 8
        else:
            res = getDetailsAssetId(priceAssetId)
            priceDecimals = int(res['decimals'])

        str_count = str(count)

        if (amountDecimals < priceDecimals):
            str_count = str_count[0:-(priceDecimals-amountDecimals)]

        if (priceDecimals == len(str_count)):
            return "0." + str_count

        if (priceDecimals < len(str_count)):
            return str_count[0:len(str_count) - priceDecimals] + "." + \
                   str_count[len(str_count) - priceDecimals:]
        if (priceDecimals > len(str_count)):
            return "0."+"0" * (priceDecimals-len(str_count)) + str_count
    else:
        return "0"


def run():
    js = getOrders()

    orders = [i for i in js[0] if len(i)]
    # print(len(orders))
    # print(orders)
    global Address
    global orders_DB
    global hash_DB

    json_dumps = json.dumps(js)
    hash_DB = hashlib.md5(json_dumps.encode("utf-8")).hexdigest()

    # print("hash_md5 = "+str(hash_DB))
    # print("json_dumps = "+str(json_dumps))

    timestamp = 0
    # ищем максимальное timestamp в orders_DB
    for order_DB in orders_DB:
        if order_DB['timestamp'] > timestamp:
            timestamp = order_DB['timestamp']

    orders_DB = []

    for order in orders:
        # print(type(order))
        # добавляем в список orders_DB все словари order
        orders_DB.append(order)
        # print(str(order))

        if (order['type'] != 7):
            continue

        if (order['timestamp'] > timestamp):
            try:
                if (order['order1']['sender'] == Address):
                    order_v = 'order1'
                elif(order['order2']['sender'] == Address):
                    order_v = 'order2'

                amount = order['amount']
                orderPrice = order[order_v]['price']
                orderAmount = order[order_v]['amount']
                amountID = order[order_v]['assetPair']['amountAsset']
                priceID = order[order_v]['assetPair']['priceAsset']

                amountAssets = getAssetsID(amountID)
                name_amount_asset = amountAssets['data']['ticker']

                priceAssets = getAssetsID(priceID)
                name_price_asset = priceAssets['data']['ticker']

                a = "{0}/{1}".format(name_amount_asset, name_price_asset)
                print("{0:-<110}".format(a))
                print("|{0: <108}|".format("Ещё есть ордера:"))
                s0 = "{0:<5}{1:<8} в кол-ве {2:>12} за {3:<14} \
                    Cтатус: Частично выполнен из {4:>4} {1:<5}" \
                    .format(order[order_v]['orderType'], name_amount_asset,
                            getCountStr(amountID, amount),
                            getPriceStr(amountID, priceID, orderPrice),
                            getCountStr(amountID, orderAmount))
                print("|{0: <108}|".format(s0))
                print("{0:-<110}".format(""))
                if(Used_Twitter):
                    postTwitt(text=s0)
            except KeyError:
                #
                print("order["+order_v+"]['sender'] = ERROR")


def loop():
    global orders_DB
    global hash_DB

    js = getOrders()
    orders = [i for i in js[0] if len(i)]

    for order in orders:
        orders_DB.append(order)

    json_dumps = json.dumps(js)
    hash_DB = hashlib.md5(json_dumps.encode("utf-8")).hexdigest()

    while True:
        try:
            # do stuff
            time.sleep(1)
            js = getOrders()
            json_dumps = json.dumps(js)
            hash_md5 = hashlib.md5(json_dumps.encode("utf-8")).hexdigest()
            print("hash_md5 = "+str(hash_md5))
            print("hash_DB  = "+str(hash_DB))

            if (hash_md5 == hash_DB):
                print("hash_md5 == hash_DB")
                continue

            if (hash_DB is not None):
                hash_DB = hash_md5
                if (Used_Sound):
                    playsound.playsound(Sound)

            run()
        except:
            s_error = "ERROR"
            print(s_error)
            if(Used_Twitter):
                postTwitt(text=s_error)


loop()
